
Ejecutar como (Java Application) la clase TodoappApplication.java dentro del paquete org.sandra.todoapp,
esto desplegará el servidor en Springboot y cargará los todos de prueba. En la consola se mostrarán
los datos de prueba con los ids que ha generado de forma autoincremental. 

Una vez hecho esto, se pueden hacer las consultas del CRUD mediante postman y las de crear y listar también por Front. 

