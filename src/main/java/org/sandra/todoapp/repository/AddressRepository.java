package org.sandra.todoapp.repository;

import org.sandra.todoapp.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;


//@Repository
public interface AddressRepository extends JpaRepository<Address, Long>{

}
