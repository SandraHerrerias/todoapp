package org.sandra.todoapp.repository.data;

import org.sandra.todoapp.model.Address;
import org.sandra.todoapp.model.Todo;
import org.sandra.todoapp.model.User;
import org.sandra.todoapp.repository.AddressRepository;
import org.sandra.todoapp.repository.UserRepository;
import org.sandra.todoapp.repository.TodoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

  private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

  @Bean
  CommandLineRunner initDatabase(AddressRepository repositoryAddress, UserRepository repositoryUser, TodoRepository repositoryTodo) {

	  


	Address address1 = new Address("De L'Agricultura", "Barcelona", "08019", "Spain"); 
	Address address2 = new Address("Padua", "Barcelona", "08023", "Spain");
	Address address3 = new Address("Isona", "Barcelona", "08030", "Spain"); 
	Address address4 = new Address("Harmonia", "Barcelona", "08035", "Spain");
	Address address5 = new Address("Bailen", "Barcelona", "08037", "Spain");
	Address address6 = new Address("Enric Granados", "Barcelona", "08008", "Spain");
	User user1 = new User("Sandra", "Herrerias", "123456", address1);
	User user2 = new User("Marta", "Marti", "12345", address2);
	User user3 = new User("Saturnino", "Montoya", "02187", address3);
	User user4 = new User("Natividad", "Pons", "651885", address4);
	User user5 = new User("Dafne", "Aranda", "524445", address5);
	User user6 = new User("Alexandra", "Vendrell", "84541", address6);

	Todo todo1 = new Todo("Pasear al perro", true, user1);
	Todo todo2 = new Todo("Preparar la cena", false, user2);
	Todo todo3 = new Todo("Recoger escritorio", false, user3);
	Todo todo4 = new Todo("Quitar el polvo", true, user4);
	Todo todo5 = new Todo("Hacer la compra", false, user2);
	Todo todo6 = new Todo("Hacer la colada", true, user5);
	Todo todo7 = new Todo("Cambiar las toallas", true, user6);
	Todo todo8 = new Todo("Planchar", false, user6);
	Todo todo9 = new Todo("Comprar juguetes perro", true, user4);
	Todo todo10 = new Todo("Limpiar baños", false, user1);
    return args -> {
//      log.info("Preloading " + repositoryAddress.save(address1));
//      log.info("Preloading " + repositoryAddress.save(address2));
      log.info("Preloading " + repositoryUser.save(user1));
      log.info("Preloading " + repositoryUser.save(user2));
      log.info("Preloading " + repositoryUser.save(user3));
      log.info("Preloading " + repositoryUser.save(user4));
      log.info("Preloading " + repositoryUser.save(user5));
      log.info("Preloading " + repositoryUser.save(user6));
      log.info("Preloading " + repositoryTodo.save(todo1));
      log.info("Preloading " + repositoryTodo.save(todo2));
      log.info("Preloading " + repositoryTodo.save(todo3));
      log.info("Preloading " + repositoryTodo.save(todo4));
      log.info("Preloading " + repositoryTodo.save(todo5));
      log.info("Preloading " + repositoryTodo.save(todo6));
      log.info("Preloading " + repositoryTodo.save(todo7));
      log.info("Preloading " + repositoryTodo.save(todo8));
      log.info("Preloading " + repositoryTodo.save(todo9));
      log.info("Preloading " + repositoryTodo.save(todo10));
    };
  }
}