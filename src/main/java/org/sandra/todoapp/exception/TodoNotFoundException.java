package org.sandra.todoapp.exception;

public class TodoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 6613445775208687628L;

	public TodoNotFoundException(Long id) {
	    super("Could not find todo " + id);
	  }
}
