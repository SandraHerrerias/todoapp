package org.sandra.todoapp.controller;

import java.util.List;

import org.sandra.todoapp.exception.TodoNotFoundException;
import org.sandra.todoapp.model.Todo;
import org.sandra.todoapp.repository.TodoRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class TodoController {

  private final TodoRepository repository;

  TodoController(TodoRepository repository) {
    this.repository = repository;
  }

  @GetMapping("/todo")
  List<Todo> all() {
    return repository.findAll();
  }
  
  @CrossOrigin(origins = "http://localhost:4200")
  @PostMapping("/todo")
  Todo newTodo(@RequestBody Todo newTodo) {
    return repository.save(newTodo);
  }
  
  @GetMapping("/todo/{id}")
  Todo one(@PathVariable Long id) {
    
    return repository.findById(id)
      .orElseThrow(() -> new TodoNotFoundException(id));
  }

  @PutMapping("/todo/{id}")
  Todo replaceAddress(@RequestBody Todo newTodo, @PathVariable Long id) {
    
    return repository.findById(id)
      .map(todo -> {
    	  todo.setTitle(newTodo.getTitle());
        todo.setCompleted(newTodo.getCompleted());
        return repository.save(todo);
      })
      .orElseGet(() -> {
        newTodo.setId(id);
        return repository.save(newTodo);
      });
  }

  @DeleteMapping("/todo/{id}")
  void deleteTodo(@PathVariable Long id) {
    repository.deleteById(id);
  }
}